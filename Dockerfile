FROM debian:stable

RUN apt-get update && apt-get install -yq --no-install-recommends unzip

COPY jdk-8u333-linux-x64.tar.gz /tmp
RUN mkdir -p /usr/lib/jvm
RUN tar -xvf /tmp/jdk-8u333-linux-x64.tar.gz -C /usr/lib/jvm

ENV JAVA_HOME=/usr/lib/jvm/jdk1.8.0_333
ENV PATH $JAVA_HOME/bin:$PATH
